package fr.epsi.workshop;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;



public class ProfessionActivity extends AppCompatActivity {
    Spinner spinner;
    Button buttonConnexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profession);

        Intent intent = getIntent();

        buttonConnexion = (Button) findViewById(R.id.bouton_form);

        buttonConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(ProfessionActivity.this, Produits.class);
                startActivity(intent1);
            }
        });




        }
    }
