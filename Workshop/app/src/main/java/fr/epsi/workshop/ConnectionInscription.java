package fr.epsi.workshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConnectionInscription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_inscription);

        Intent intent = getIntent();

        Button button = findViewById(R.id.page_connect_inscr_boutton_connexion);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(ConnectionInscription.this,ConnectionActivity.class);
                startActivity(intent1);
            }
        });
    }
}